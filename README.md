# Basicos do GIT

## Clone

Fazer cópia do repositório que está no servidor (gitlab, github, bitbucket) para a sua máquina (local)

## Add

Adicionar arquivos modificados ao próximo commit que está sendo feito

## Commit

Salvar modificações adicionadas (add) e dar uma mensagem descritiva sobre elas

Checkpoint

## Push

Enviar commits para o servidor

## Status

Mostra modificações locais no branch atual

## Diff

Diferenças por linha nos arquivos modificados

# Trabalhando com Branchs

## Criando um branch

git checkout -b NOMEDOBRANCH
